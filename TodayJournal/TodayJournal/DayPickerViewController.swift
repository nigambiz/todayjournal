//
//  DayPickerViewController.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/3/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class DayPickerViewController: UIViewController {

	private static var _selectedDate:NSDate = NSDate()

	static func getSelectedDate() -> NSDate {
		return _selectedDate
	}


	@IBOutlet weak var datePicker: UIDatePicker!

	private var _dayViewController:DayViewController?

	@IBAction func onCancel(sender: UIButton) {
		println("date cancel")
	}

	@IBAction func onToday(sender: UIButton) {
		datePicker.date = NSDate()
	}

	@IBAction func onDone(sender: UIButton) {
		DayPickerViewController._selectedDate = datePicker.date
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
	override func viewWillAppear(animated: Bool) {
		datePicker.date = DayPickerViewController.getSelectedDate()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
