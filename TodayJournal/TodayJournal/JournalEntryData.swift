//
//  JournalEntryData.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/1/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

struct JournalEntryData {
	var createdAt: Int = 0
	var date: Int = 0
	var timeInterval: Int = 0
	
	var bodyText: String = ""
	var locationString: String = ""

	// weather stuff
	var weatherTemperature: Int = 0
	var weatherDescription: String = ""
	var weatherIcon: String = ""

	// image
	var bodyImage: UIImage? = nil

}