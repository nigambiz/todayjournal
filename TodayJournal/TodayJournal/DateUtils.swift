//
//  DateUtils.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/1/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import Foundation

class DateUtils: NSObject {

	private static let s_timeZoneUTC = NSTimeZone(abbreviation: "GMT")
	private static let s_localTimeZone = NSTimeZone.localTimeZone()
	private static let s_calendar = NSCalendar.currentCalendar()

	static func getMidnightTimestampForDate(var date: NSDate?) -> Int {

		// if nil was passed for date, use "now"
		if date == nil {
			date = NSDate()
		}

		let components = s_calendar.componentsInTimeZone(s_localTimeZone, fromDate: date!)
		components.hour = 0;
		components.minute = 0;
		components.second = 0;
		components.timeZone = s_timeZoneUTC

		let newDate = s_calendar.dateFromComponents(components)
		let result = Int(newDate!.timeIntervalSince1970)
		return result
	}

	static func getTimestampCompenents(date: NSDate) -> (now: Int, midnight: Int, timeInterval: Int) {

		let fullTimestamp = Int(date.timeIntervalSince1970)
		let midnightTimestamp = getMidnightTimestampForDate(date)
		let timeInterval = fullTimestamp - midnightTimestamp

		let result = (fullTimestamp, midnightTimestamp, timeInterval)
		return result
	}

	static func getLongDisplayStringForDate(date: NSDate) -> String {

		let formatter = NSDateFormatter()
		formatter.dateFormat = "EEEE, MMMM d, yyyy"
		let result = formatter.stringFromDate(date)
		return result
	}

	static func getTimeDisplayStringForDate(date: NSDate) -> String {

		let formatter = NSDateFormatter()
		formatter.AMSymbol = "am"
		formatter.PMSymbol = "pm"
		formatter.dateFormat = "h:mma"

		let result = formatter.stringFromDate(date)
		return result
	}

	static func getTimeDisplayStringForTimestamp(timestamp: Int) -> String {
		let date = NSDate(timeIntervalSince1970: Double(timestamp))
		let result = getTimeDisplayStringForDate(date)
		return result
	}



}
