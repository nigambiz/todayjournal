//
//  FirstViewController.swift
//  TodayJournal
//
//  Created by Nigam Shah on 7/27/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class DayViewController: UIViewController, UIToolbarDelegate {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var dayButton: UIButton!

	private var _dayViewDelegate: DayViewDelegate = DayViewDelegate()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.

		println("DayViewController.viewDidLoad()")

		ServiceLocator.initServices();
		_test()
		_dayViewDelegate.viewDidLoad(tableView)
	}

	override func viewWillAppear(animated: Bool) {
		let selectedDate = DayPickerViewController.getSelectedDate()
		let dayString = DateUtils.getLongDisplayStringForDate(selectedDate)
		dayButton.setTitle(dayString, forState: UIControlState.Normal)
		_dayViewDelegate.initDay(selectedDate)
		_dayViewDelegate.viewWillAppear()
	}


	private func _test() {

	}

	@IBAction func onDayButton(sender: UIButton) {
		println("choose day")
	}

	private func _initMockData() {

		let mockData = MockData()
		mockData.initMockData()
		mockData.saveDataToStorage()

	}

	func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
		return UIBarPosition.TopAttached
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}

