//
//  StorageService.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/1/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class StorageService: NSObject {

	private var useCoreData = true

	private let _coreDataService = CoreDataService()

	func saveJournaEntry(entry: JournalEntryData) -> Bool {
		let entries = [entry]
		let success = saveJournalEntries(entries)
		return success
	}

	func saveJournalEntries(entries: [JournalEntryData]) -> Bool {

		if useCoreData {
			let success = _coreDataService.saveJournalEntries(entries)
			return success
		}

		println("No storage implement chosen, could not save")
		return false
	}

	func getAllJournalEntryData() -> [JournalEntryData] {

		if useCoreData {
			let result = _coreDataService.fetchAllJournalEntryData()
			return result
		}

		println("No storage implement chosen, returning an empty list")
		return [JournalEntryData]()
	}

	func getJournalEntryDataForDate(date: NSDate?) -> [JournalEntryData] {

		// get midnight timestamp for date
		let timestamp = DateUtils.getMidnightTimestampForDate(date)

		if useCoreData {
			let result = _coreDataService.fetchJournalEntryDataForDate(timestamp)
			return result
		}

		println("No storage implement chosen, returning an empty list")
		return [JournalEntryData]()
	}
   
}
