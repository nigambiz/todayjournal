//
//  JournalCellPresenter.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/2/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class JournalCellPresenter: NSObject {

	private var _data:JournalEntryData

	init(data:JournalEntryData) {
		_data = data
	}

	func getCellType() -> JournalCellType {
		if (!_data.bodyText.isEmpty) {
			return JournalCellType.Text
		}

		// using .Text as a default for now
		return JournalCellType.Text
	}

	func getBodyText() -> String? {
		return _data.bodyText
	}

	func getLocationText() -> String {
		return _data.locationString
	}

	func getTimeText() -> String {
		let timestamp = _data.date + _data.timeInterval // need to take Daylight savings into account
		let result = DateUtils.getTimeDisplayStringForTimestamp(timestamp)
		return result
	}

	func getTimeInterval() -> Int {
		return _data.timeInterval
	}

	func getWeatherTemp() -> String {
		if (_data.weatherTemperature > 0) {
			return String(_data.weatherTemperature) + "\u{00B0} F"
		}
		return ""
	}
	func getWeatherDescription() -> String {
		return _data.weatherDescription.capitalizedString
	}

	func getWeatherIconName() -> String? {
		let result: String? = _data.weatherIcon.isEmpty ? nil : _data.weatherIcon
		return result
	}

	func getBodyImage() -> UIImage? {
		return _data.bodyImage
	}

	func dispose() {
		_data = JournalEntryData()
	}
}
