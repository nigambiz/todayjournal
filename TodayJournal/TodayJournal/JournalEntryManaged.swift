//
//  JournalEntryManaged.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/16/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import Foundation
import CoreData

class JournalEntryManaged: NSManagedObject {

    @NSManaged var bodyText: String
    @NSManaged var createdAt: NSNumber
    @NSManaged var date: NSNumber
    @NSManaged var locationString: String
    @NSManaged var timeInterval: NSNumber
    @NSManaged var weatherDescription: String
    @NSManaged var weatherIcon: String
    @NSManaged var weatherTemperature: NSNumber
    @NSManaged var bodyImage: NSData?

}
