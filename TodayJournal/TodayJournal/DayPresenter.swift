//
//  DayViewModel.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/2/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class DayPresenter: NSObject {

	private var _date:NSDate?

	private var _cellPresenterCollection: [JournalCellPresenter]

	init(date:NSDate) {
		_date = date
		_cellPresenterCollection = [JournalCellPresenter]()
	}

	func loadAndInitializeData() {

		let entryDataCollection = ServiceLocator.storageService.getJournalEntryDataForDate(_date)
		for entryData in entryDataCollection {
			addEntryData(entryData)
		}
		sortPresenters()
	}

	func addEntryData(entryData: JournalEntryData) {
		let presenter = JournalCellPresenter(data: entryData)
		_cellPresenterCollection.append(presenter)
	}

	func sortPresenters() {

		_cellPresenterCollection.sort {
			let isOrderedBefore = $0.getTimeInterval() < $1.getTimeInterval()
			return isOrderedBefore
		}
	}

	func getNumberOfJournalEntries() -> Int {
		let result = _cellPresenterCollection.count;
		return result
	}

	func getJournalCellPresenter(#section: Int, row: Int) -> JournalCellPresenter? {

		if section == 0 {
			if row >= 0 && row < _cellPresenterCollection.count {
				let result = _cellPresenterCollection[row]
				return result
			}
		}
		
		return nil;
	}


	func dispose() {
		_date = nil;
		for presenter in _cellPresenterCollection {
			presenter.dispose()
		}

		_cellPresenterCollection = [JournalCellPresenter]()
	}



}
