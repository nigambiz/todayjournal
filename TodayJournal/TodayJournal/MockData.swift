//
//  MockDataFactory.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/1/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class MockData: NSObject {

	// using an array literal for everything is crashing xCode,
	// so i will use one entry to initialize the array, 
	// and i am appending the rest of the mock data in a function

	private var _data = [

		[
			"createdAt": 1438219400,
			"date": 1438473600,
			"timeInterval": 36000,
			"id": 5,
			"bodyText": "This is yesterday's first entry",
			"locationString": "Springfield, NJ"
		],
	]

	func initMockData() {

		// using an array literal for everything is crashing xCode,
		// i am appending the mock data in a function
		_data.append([
			"createdAt": 1438460071,
			"date": 1438473600,
			"timeInterval": 36000,
			"id": 6,
			"bodyText": "This is my first entry",
			"locationString": "New York, NY"
		])

		_data.append([
			"createdAt": 1438461075,
			"date": 1438473600,
			"timeInterval": 44731,
			"id": 7,
			"bodyText": "This is my second entry",
			"locationString": "New York, NY"
		])

		_data.append([
			"createdAt": 1438462081,
			"date": 1438473600,
			"timeInterval": 54888,
			"id": 8,
			"bodyText": "This is my third entry",
			"locationString": "New York, NY"
		])

	}

	func saveDataToStorage() -> Bool {

		let storageService = ServiceLocator.storageService
		let entries = getJournalEntryData()


		var success = storageService.saveJournalEntries(entries);
		println("Mock data success \(success)")

		return success
	}

	func getJournalEntryData() -> [JournalEntryData] {

		var result = [JournalEntryData]()

		var journalEntry: JournalEntryData

		for dataObject in _data {

//			journalEntry = JournalEntryData(
//				createdAt: dataObject["createdAt"] as! Int,
//				date: dataObject["date"] as! Int,
//				timeInterval: dataObject["timeInterval"] as! Int,
//				bodyText: dataObject["bodyText"] as! String,
//				locationString: dataObject["locationString"] as! String,
//				weatherTemperature: 87,
//				weatherDescription: "scattered clouds",
//				weatherIcon: "03d"
//			)
//
//			result.append(journalEntry)

		}

		return result
	}
}
