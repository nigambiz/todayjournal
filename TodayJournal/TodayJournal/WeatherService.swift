//
//  WeatherService.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/3/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import CoreLocation
import Alamofire
import SwiftyJSON


class WeatherService: NSObject {

	private let kRootApiUrl = "http://api.openweathermap.org/data/2.5"
	private let kAPIKey = "43c1054bc22940e482173ba5fe0dd3c3"

	private let kMinimumUpdateIntervalInSeconds = 1 * 60 * 60 // 1 hour

	private var _lastUpdate: NSDate?
	private var _requestInProgress:Bool = false
	private var _currentWeatherInfo:WeatherInfo?

	func updateWeatherFromCurrentLocation() {

		if _requestInProgress || (_lastUpdate != nil && Int(-1 * _lastUpdate!.timeIntervalSinceNow) < kMinimumUpdateIntervalInSeconds) {
			return
		}

		if let location = ServiceLocator.locationService.getCurrentLocation() {
			let latitude = location.coordinate.latitude
			let longitude = location.coordinate.longitude
			_updateWeatherByCoordinates(latitude: latitude, longitude: longitude)
		} else {
			println("could not update weather because we don't have a current location")
		}
	}

	func getCurrentWeatherInfo() -> WeatherInfo? {
		return _currentWeatherInfo
	}

	private func _updateWeatherByCoordinates(#latitude: CLLocationDegrees, longitude: CLLocationDegrees) {

		let url = "http://api.openweathermap.org/data/2.5/weather"
		let params: [String: AnyObject] = ["APPID=": kAPIKey, "lat":latitude, "lon":longitude, "units": "imperial"]
		println(params)

		_requestInProgress = true

		Alamofire.request(.GET, url, parameters: params)
			.responseJSON { (request, response, json, error) in
				self._requestInProgress = false
				if(error != nil) {
					println("Error: \(error)")
					println(request)
					println(response)
				} else {
					println("Success: \(url)")
					self._lastUpdate = NSDate()
					var jsonObject = JSON(json!)
					self._processWeatherResponse(jsonObject)
				}
			}
	}

	private func _processWeatherResponse(jsonObject: JSON) {

		// create weather info
		var currentWeatherInfo = WeatherInfo()

		if let dt = jsonObject["dt"].int {
			currentWeatherInfo.timestamp = dt;
		}

		if let main = jsonObject["main"].dictionary {
			if let temp = main["temp"]?.float {
				currentWeatherInfo.temperature = Int(round(temp))
			}
		}

		// if both the timestamp and temp are there, it is valid
		if !(currentWeatherInfo.timestamp > 0 && currentWeatherInfo.temperature > 0) {
			return;
		}
		
		if let weatherArray = jsonObject["weather"].array {

			if let weather = weatherArray[0].dictionary {
				if let desc = weather["description"]?.string {
					currentWeatherInfo.description = desc
				}
				if let icon = weather["icon"]?.string {
					currentWeatherInfo.icon = icon
				}
			}
		}
		_currentWeatherInfo = currentWeatherInfo
	}





}
