//
//  ServiceLocator.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/1/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class ServiceLocator: NSObject {

	static let storageService = StorageService()
	static let locationService = LocationService()
	static let weatherService = WeatherService()
	static let cameraService = CameraService()


	static func initServices() {
		locationService
		storageService
		weatherService
		cameraService
	}
}
