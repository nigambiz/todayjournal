//
//  CameraService.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/11/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit
import MobileCoreServices

class CameraService: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

	typealias ProcessImageHandler = (UIImage) -> Void
	typealias DismissImagePickerHandler = (UIImagePickerController) -> Void

	private var completionHandlers = [UIImagePickerController: (ProcessImageHandler, DismissImagePickerHandler)]()


	func createCameraController(#processHandler: ProcessImageHandler, dismissHandler: DismissImagePickerHandler) -> UIImagePickerController? {
		if doesCameraSupportTakingPhotos() {
			let picker = UIImagePickerController()
			picker.sourceType = .Camera
			picker.allowsEditing = true
			picker.delegate = self
			completionHandlers[picker] = (processHandler, dismissHandler)
			return picker
		} else {
			println("camera does not support photos")
			return nil
		}
	}

	func createPhotoLibraryController(#processHandler: ProcessImageHandler, dismissHandler: DismissImagePickerHandler) -> UIImagePickerController? {
		let picker = UIImagePickerController()
		picker.sourceType = .PhotoLibrary
		picker.allowsEditing = true
		picker.delegate = self
		completionHandlers[picker] = (processHandler, dismissHandler)
		return picker
	}

	func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
		if let handlerPair = completionHandlers[picker] {

			completionHandlers[picker] = nil // remove the handlers from the dictionary

			let processHandler = handlerPair.0
			let dismissHandler = handlerPair.1
			let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage
			let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage

			if let finalImage = editedImage {
				processHandler(finalImage)
			} else if let finalImage = originalImage {
				processHandler(finalImage)
			} else {
				println("no image")
			}
			dismissHandler(picker)
		}
	}
	func imagePickerControllerDidCancel(picker: UIImagePickerController) {
		println("image picker cancelled")

		if let handlerPair = completionHandlers[picker] {

			completionHandlers[picker] = nil // remove the handlers from the dictionary

			let dismissHandler = handlerPair.1
			dismissHandler(picker)
		}

	}

	func doesCameraSupportTakingPhotos() -> Bool {
		let result = _cameraSupportsMedia(kUTTypeImage as String, sourceType: .Camera)
		return result
	}

	func doesCameraSupportShootingVideos() -> Bool{
		let result = _cameraSupportsMedia(kUTTypeMovie as String, sourceType: .Camera)
		return result
	}

	private func _cameraSupportsMedia(mediaType: String,	sourceType: UIImagePickerControllerSourceType) -> Bool {	

		let availableMediaTypes = UIImagePickerController.availableMediaTypesForSourceType(sourceType) as! [String]

		for type in availableMediaTypes {
			if type == mediaType {
				return true
			}
		}

		return false
	}

	

}
