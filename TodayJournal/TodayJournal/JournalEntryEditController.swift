//
//  JournalEntryEditController.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/2/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit
import MobileCoreServices

class JournalEntryEditController: UIViewController, UITextViewDelegate {


	@IBOutlet weak var saveButton: UIButton!
	@IBOutlet weak var cancelButton: UIButton!

	@IBOutlet weak var bodyTextView: UITextView!
	@IBOutlet weak var bodyImageView: UIImageView!


	// body text defaults
	private let kDefaultBodyTextColor:UIColor = UIColor.lightGrayColor()
	private let kDefaultBodyText = "Dear Diary..."

	// image default
	private let kDefaultImage = UIImage(named: "image_placeholder")

    override func viewDidLoad() {
        super.viewDidLoad()

		bodyTextView.delegate = self;

		//Looks for single or multiple taps.
		var tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
		view.addGestureRecognizer(tap)

		tap = UITapGestureRecognizer(target: self, action: "onImageTap")
		bodyImageView.addGestureRecognizer(tap)

		NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown", name: UIKeyboardDidShowNotification, object: nil)

		NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden", name: UIKeyboardWillHideNotification, object: nil)

		_resetAllFields()
    }

	override func viewDidDisappear(animated: Bool) {
//		dispose()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	func keyboardWasShown() {
		_disableActionButtons()
	}

	func keyboardWillBeHidden() {
		let bodyText = bodyTextView.text
		if (bodyText.isEmpty) {
			_resetBodyText()
		}

		_conditionallyEnableActionButtons()
	}

	private func _conditionallyEnableActionButtons() {

		var shouldEnable =
			_hasBodyText() ||
			_hasBodyImage()

		// conditionally enable
		if (shouldEnable) {
			_enableActionButtons()
		} else {
			_disableActionButtons()
		}

	}

	private func _hasBodyText() -> Bool {
		let bodyText = bodyTextView.text;
		var result = !bodyText.isEmpty && bodyText != kDefaultBodyText
		return result
	}

	private func _hasBodyImage() -> Bool {
		let result = bodyImageView.image != kDefaultImage
		return result
	}

	private func _enableActionButtons() {
		saveButton.enabled = true
		cancelButton.enabled = true
	}

	private func _disableActionButtons() {
		saveButton.enabled = false
		cancelButton.enabled = false
	}

	//Calls this function when the tap is recognized.
	func dismissKeyboard() {
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}

	func onImageTap() {
		println("onImageTap")

		if !_hasBodyImage() {
			_showPhotoActionSheet()
		}

	}

	private func _showPhotoActionSheet() {
		let alertController = UIAlertController(title: "Add Photo", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)

		var action = UIAlertAction(
			title: "Take Photo",
			style: UIAlertActionStyle.Default,
			handler: { (paramAction:UIAlertAction!) in
				println("Take Photo")
				self._launchCameraController()
		})

		alertController.addAction(action)

		action = UIAlertAction(
			title: "Choose Existing",
			style: UIAlertActionStyle.Default,
			handler: { (paramAction:UIAlertAction!) in
				println("Choose Existing")
				self._launchPhotoLibraryController()
			}
		)

		alertController.addAction(action)

		action = UIAlertAction(
			title: "Cancel",
			style: UIAlertActionStyle.Cancel,
			handler: { (paramAction:UIAlertAction!) in
				println("Cancel")
			}
		)

		alertController.addAction(action)

		presentViewController(alertController, animated: true, completion: nil)

	}

	private func _setPhoto(image: UIImage) {
		println("image picker complee")
		bodyImageView.image = image
	}

	private func _dismissPicker(picker: UIImagePickerController) {
		println("image picker dismiss")
		picker.dismissViewControllerAnimated(true, completion: nil)
		_conditionallyEnableActionButtons()
	}

	private func _launchCameraController() {
		let cameraControllerOpt = ServiceLocator.cameraService.createCameraController(processHandler: _setPhoto, dismissHandler: _dismissPicker)
		if let cameraController = cameraControllerOpt {
			presentViewController(cameraController, animated: true, completion: nil)
		}
	}
	private func _launchPhotoLibraryController() {
		let photoControllerOpt = ServiceLocator.cameraService.createPhotoLibraryController(processHandler: _setPhoto, dismissHandler: _dismissPicker)
		if let photoController = photoControllerOpt {
			presentViewController(photoController, animated: true, completion: nil)
		}
	}

	func textViewDidBeginEditing(textView: UITextView) {
		if textView.textColor == kDefaultBodyTextColor {
			textView.text = nil
			textView.textColor = UIColor.darkTextColor()
		}
	}

	private func _resetAllFields() {
		_resetBodyText()
		_resetImageView()
		_disableActionButtons()
	}
	private func _resetBodyText() {
		bodyTextView.text = kDefaultBodyText
		bodyTextView.textColor = kDefaultBodyTextColor
	}
	private func _resetImageView() {
		bodyImageView.image = kDefaultImage
	}

	@IBAction func onSaveClick(sender: UIButton) {
		_saveFields()
	}

	private func _saveFields() {
		let now = NSDate()
		let timestamps = DateUtils.getTimestampCompenents(now)
		let bodyText = bodyTextView.text

		let locationString = ServiceLocator.locationService.getCurrentLocationForDisplay()

		// weather stuff
		let weatherInfo = ServiceLocator.weatherService.getCurrentWeatherInfo()
		let temp:Int
		let desc:String
		let icon:String

		// image stuff
		var image: UIImage? = bodyImageView.image
		if image == kDefaultImage {
			image = nil
		}


		if let info = weatherInfo {
			temp = info.temperature
			desc = info.description
			icon = info.icon
		} else {
			temp = 0
			desc = ""
			icon = ""
		}

		let data = JournalEntryData(
			createdAt: timestamps.now,
			date: timestamps.midnight,
			timeInterval: timestamps.timeInterval,
			bodyText: bodyText,
			locationString: locationString,
			weatherTemperature: temp,
			weatherDescription: desc,
			weatherIcon: icon,
			bodyImage: image
		)

		let success = ServiceLocator.storageService.saveJournaEntry(data)
		println("saved = \(success)")

		if success {
			_resetAllFields()
		}

	}
	@IBAction func onCancelClick(sender: UIButton) {
		_resetAllFields()
	}

	func dispose() {
		_resetAllFields()
	}

}
