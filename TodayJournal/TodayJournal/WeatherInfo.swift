//
//  WeatherInfo.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/3/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import Foundation

struct WeatherInfo {

	var temperature: Int = 0
	var timestamp: Int = 0
	var description: String = ""
	var icon: String = ""

}