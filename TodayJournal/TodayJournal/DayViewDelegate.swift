//
//  DayTableViewController.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/2/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class DayViewDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {

	private var _dayPresenter: DayPresenter?
	private var _view: UITableView?


	func viewDidLoad(view: UITableView) {

		_view = view
		_view!.delegate = self
		_view!.dataSource = self
		_view!.estimatedRowHeight = 50

		initCellTemplates()
    }

	func viewWillAppear() {
		_view!.reloadData()
	}

	func initDay(date: NSDate) {
		if _dayPresenter != nil {
			_dayPresenter!.dispose()
			_dayPresenter = nil
		}

		_dayPresenter = DayPresenter(date: date)
		_dayPresenter!.loadAndInitializeData()
	}

	func initCellTemplates() {
		// register the cell view templates
		var nib = UINib(nibName: "JournalCellViewTextImage", bundle:nil)
		_view!.registerNib(nib, forCellReuseIdentifier: JournalCellType.Text.rawValue)

	}

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.

		var result: Int = 0;

		if section == 0 && _dayPresenter != nil {
			result = _dayPresenter!.getNumberOfJournalEntries()
		}

        return result
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		let cell: JournalCellViewBase

		let cellPresenter = _dayPresenter!.getJournalCellPresenter(section: indexPath.section, row: indexPath.row)

		if let presenter = cellPresenter {
			let type = presenter.getCellType()

			cell = tableView.dequeueReusableCellWithIdentifier(type.rawValue, forIndexPath: indexPath) as! JournalCellViewBase

			cell.populate(presenter);
		} else {
			cell = tableView.dequeueReusableCellWithIdentifier(JournalCellType.Text.rawValue, forIndexPath: indexPath) as! JournalCellViewBase
		}


		return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
