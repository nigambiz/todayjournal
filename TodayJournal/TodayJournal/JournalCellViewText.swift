//
//  JournalCellViewText.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/1/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit
class JournalCellViewText: JournalCellViewBase {

	@IBOutlet weak var bodyLabel: UILabel!

	@IBOutlet weak var weatherLabel: UILabel!

	@IBOutlet weak var locationLabel: UILabel!

	@IBOutlet weak var weatherIcon: UIImageView!

	@IBOutlet weak var timeLabel: UILabel!

	override func populate(presenter: JournalCellPresenter) {
		let weatherText = presenter.getWeatherTemp() + "\n" + presenter.getWeatherDescription()
		populate(
			timeText: presenter.getTimeText(),
			locationText: presenter.getLocationText(),
			bodyText: presenter.getBodyText(),
			weatherText: weatherText,
			weatherIconName: presenter.getWeatherIconName()
		)
	}

	func populate(#timeText: String, locationText: String, bodyText: String?, weatherText: String?, weatherIconName: String?) {
		timeLabel.text = timeText
		locationLabel.text = locationText

		bodyLabel.text = bodyText != nil ? bodyText : "no text"

		weatherLabel.text = weatherText != nil ? weatherText : ""

		if let iconName = weatherIconName {
			weatherIcon.image = UIImage(named: iconName)
		} else {
			weatherIcon.image = nil
		}
	}

}
