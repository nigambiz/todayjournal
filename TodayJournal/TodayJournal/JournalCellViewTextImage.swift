//
//  JournalCellViewTextImage.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/9/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit

class JournalCellViewTextImage: JournalCellViewBase {

	@IBOutlet weak var bodyImage: UIImageView!
	
	@IBOutlet weak var bodyLabel: UILabel!

	@IBOutlet weak var weatherLabel: UILabel!
	
	@IBOutlet weak var weatherIcon: UIImageView!

	@IBOutlet weak var locationLabel: UILabel!

	@IBOutlet weak var timeLabel: UILabel!

	override func populate(presenter: JournalCellPresenter) {
		let weatherText = presenter.getWeatherTemp() + "\n" + presenter.getWeatherDescription()
		populate(
			timeText: presenter.getTimeText(),
			locationText: presenter.getLocationText(),
			bodyText: presenter.getBodyText(),
			weatherText: weatherText,
			weatherIconName: presenter.getWeatherIconName(),
			bodyImage: presenter.getBodyImage()
		)

	}

	func populate(#timeText: String, locationText: String, bodyText: String?, weatherText: String?, weatherIconName: String?, bodyImage: UIImage?) {
		timeLabel.text = timeText
		locationLabel.text = locationText

		bodyLabel.text = bodyText != nil ? bodyText : "no text"

		weatherLabel.text = weatherText != nil ? weatherText : ""

		if let iconName = weatherIconName {
			weatherIcon.image = UIImage(named: iconName)
		} else {
			weatherIcon.image = nil
		}
		if let image = bodyImage {
			_setMainImage(image)
		} else {
			self.bodyImage.image = nil
		}
	}

	private func _setMainImage(url: String) {

		var name:String
		name = "diary_coffee"

		let image = UIImage(named: name)
	}

	private func _setMainImage(image: UIImage) {

		let fixedW = bodyLabel.bounds.width
		let newImage = resizeImageToWidth(image, newWidth: fixedW)
		bodyImage.image = newImage

		println("setMainImage")

	}

	func resizeImageToWidth(image: UIImage, newWidth: CGFloat) -> UIImage {

		let w = image.size.width
		let h = image.size.height
		let ratio = newWidth / w
		let newHeight = h * ratio

		UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
		image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		return newImage
	}



}
