//
//  JournalCellType.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/2/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import Foundation

enum JournalCellType : String {
	case None = "None"
	case Text = "JournalCellType.Text"
}