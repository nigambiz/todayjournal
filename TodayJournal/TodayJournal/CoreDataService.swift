//
//  CoreDataService.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/1/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit
import CoreData

class CoreDataService: NSObject {

	private let _kJournalEntryEntityName = "JournalEntry"

	private let _managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

	func fetchAllJournalEntryData() -> [JournalEntryData] {
		let result = fetchJournalEntryDataForDate(0)
		return result
	}

	func fetchJournalEntryDataForDate(dateTimestamp:Int) -> [JournalEntryData] {
		var result = [JournalEntryData]()

		let fetchResult = _fetchManagedJournalEntriesForDate(dateTimestamp)

		if let objects = fetchResult {
			for object in objects {
				var entryData = managedObjectToData(object)
				result.append(entryData)
			}
		}

		return result
	}

	func saveJournalEntries(entries: [JournalEntryData]) -> Bool {

		let entityDescription = _getJournalEntityDescription();

		for entryData in entries {
			var entryObject = JournalEntryManaged(entity: entityDescription!, insertIntoManagedObjectContext: _managedObjectContext!)

			populateWithData(entryObject, data: entryData)
		}

		var error: NSError?

		_managedObjectContext?.save(&error)

		if let err = error {
			println(err.localizedDescription)
			return false
		}

		return true
	}


	func populateWithData(object: JournalEntryManaged, data: JournalEntryData) {

		object.createdAt = data.createdAt
		object.date = data.date
		object.timeInterval = data.timeInterval
		object.bodyText = data.bodyText
		object.locationString = data.locationString
		object.weatherTemperature = data.weatherTemperature
		object.weatherDescription = data.weatherDescription
		object.weatherIcon = data.weatherIcon

		if let image = data.bodyImage {
			object.bodyImage = UIImageJPEGRepresentation(image, 0.8)
		}
	}

	func managedObjectToData(object: JournalEntryManaged) -> JournalEntryData {
		var image: UIImage? = nil

		if let imageData = object.bodyImage {
			image = UIImage(data: imageData)
		}


		let result = JournalEntryData(
			createdAt: object.createdAt as! Int,
			date: object.date as! Int,
			timeInterval: object.timeInterval as! Int,
			bodyText: object.bodyText,
			locationString: object.locationString,
			weatherTemperature: object.weatherTemperature as! Int,
			weatherDescription: object.weatherDescription,
			weatherIcon: object.weatherIcon,
			bodyImage: image
		)

		return result
	}

	private func _fetchManagedJournalEntriesForDate(dateTimestamp: Int) -> [JournalEntryManaged]? {

		let dateNumber = NSNumber(integer: dateTimestamp)

		let entityDescription = _getJournalEntityDescription();

		let request = NSFetchRequest()
		request.entity = entityDescription;

		if dateTimestamp > 0 {
			let pred = NSPredicate(format: "(date = %@)", dateNumber)
			request.predicate = pred
		}

		var error: NSError?

		var objects = _managedObjectContext?.executeFetchRequest(request, error: &error)

		let result = objects as? [JournalEntryManaged]
		return result
	}

	private func _getJournalEntityDescription() -> NSEntityDescription? {

		let entityDescription = NSEntityDescription.entityForName(_kJournalEntryEntityName, inManagedObjectContext: _managedObjectContext!);

		return entityDescription;
	}
	
}
