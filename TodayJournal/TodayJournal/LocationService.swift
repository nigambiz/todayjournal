//
//  LocationService.swift
//  TodayJournal
//
//  Created by Nigam Shah on 8/2/15.
//  Copyright (c) 2015 Nigam Shah. All rights reserved.
//

import UIKit
import CoreLocation

class LocationService: NSObject, CLLocationManagerDelegate {

	private let _locationManager = CLLocationManager()
	private var _currentLocation: CLLocation?
	private var _currentPlacemark: CLPlacemark?

	override init() {
		super.init()

		println("LocationService.init")

		_currentLocation = nil;

		//_locationManager.desiredAccuracy = kCLLocationAccuracyBest // for debugging

		_locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
		_locationManager.delegate = self
		_locationManager.requestWhenInUseAuthorization()
		_locationManager.startUpdatingLocation()

	}


	func getCurrentLocationForDisplay() -> String {

		var result = "";

		if let placemark = _currentPlacemark {


			if (placemark.locality != nil && !placemark.locality.isEmpty) {
				result += placemark.locality
				if (placemark.administrativeArea != nil && !placemark.administrativeArea.isEmpty) {
					result += ", " + placemark.administrativeArea
				}
			} else if (placemark.administrativeArea != nil && !placemark.administrativeArea.isEmpty) {
				result += placemark.administrativeArea
			}
		}

		return result
	}

	func getCurrentLocation() -> CLLocation? {
		return _currentLocation;
	}

	func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {

		var latestLocation: AnyObject = locations[locations.count - 1]

		// set currentLocation
		if let newLocation: CLLocation = latestLocation as? CLLocation {
			if (newLocation != _currentLocation) {

				// we have moved!
				// set the new location
				_currentLocation = newLocation

				// current placemark is now invalid
				_currentPlacemark = nil

				// try to get a new placemark
				_requestPlacemark()

				// update the weather
				// TODO: Make this triggered by a notification or event
				ServiceLocator.weatherService.updateWeatherFromCurrentLocation()
			}
		}

	}

	private func _requestPlacemark() {
		if let location = _currentLocation {
			CLGeocoder().reverseGeocodeLocation(_currentLocation, completionHandler: {(placemarks, error) -> Void in
				if error != nil {
					println("Reverse geocoder failed with error" + error.localizedDescription)
					return
				}

				if placemarks.count > 0 {
					let placemark = placemarks[0] as! CLPlacemark
					self._currentPlacemark = placemark;
				} else {
					println("Problem with the data received from geocoder")
				}
			})
		}


	}

	func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
		// fail silently for now
	}

}
